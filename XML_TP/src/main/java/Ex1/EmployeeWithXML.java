package Ex1;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.QName;
import org.dom4j.io.SAXReader;

public class EmployeeWithXML implements XMLEmployeeService {

	@Override
	public Document EmployeetoXML(Employee employe) {
		// TODO Auto-generated method stub
		Document document = DocumentHelper.createDocument();
		Element rootElement = document.addElement("user");
		rootElement.addAttribute("id", String.valueOf(employe.getId()));
		Element nameElement = rootElement.addElement("name");
		nameElement.addText(employe.getName());
		Element ageElement = rootElement.addElement("age");
		ageElement.addText(String.valueOf(employe.getAge()));
		return document;
	}

	public Employee FileXMLtoEmployee(String fileName) {
		// TODO Auto-generated method stub
		try (Reader reader = new FileReader(fileName);) {
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(reader);
			
			Employee user = new Employee();
			Element rootElement = document.getRootElement();
			
			Attribute attributeID = rootElement.attribute("id");
			long id = Long.parseLong(attributeID.getValue());
			user.setId(id);
			
			//Namespace namespace = new Namespace("t3", "http://paumard.org/t3");
			//QName nameQName = new QName("name", namespace);
			//Element nameElement = rootElement.element(nameQName);
			//String name = nameElement.getText();
			
			user.setName(rootElement.elementText("name"));
			
			
			//QName ageQName = new QName("age", namespace);
			//Element ageElement = rootElement.element(ageQName);
			//int age = Integer.parseInt(ageElement.getText());
			
			int age = Integer.parseInt(rootElement.elementText("age"));
			user.setAge(age);
			
			return user;
			
		} catch (IOException | DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Employee XMLtoEmployee(Document doc) {
		// TODO Auto-generated method stub
		Employee user = new Employee();
		Element rootElement = doc.getRootElement();
		Attribute attributeID = rootElement.attribute("id");
		long id = Long.parseLong(attributeID.getValue());
		user.setId(id);
		user.setName(rootElement.elementText("name"));
		int age = Integer.parseInt(rootElement.elementText("age"));
		user.setAge(age);
		return user;
	}

}
