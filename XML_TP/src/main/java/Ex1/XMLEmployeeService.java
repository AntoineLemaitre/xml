package Ex1;

import org.dom4j.Document;

public interface XMLEmployeeService {

	Document EmployeetoXML(Employee employe);
	Employee XMLtoEmployee(Document doc);
}
