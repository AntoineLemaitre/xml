package Ex3;

import java.io.FileReader;
import java.io.Reader;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class handlerXML extends DefaultHandler {
	

	public void RootIs() {
		//Reader reader = new FileReader(fileName);
		//SAXReader saxReader = new SAXReader();
		//Document document = saxReader.read(reader);
		//Element rootElement = document.getRootElement();
		//System.out.println("La racine est"+);
	}
	@Override
	public void startDocument() throws SAXException {
		System.out.println("Début du document");
	}

	@Override
	public void endDocument() throws SAXException {
		System.out.println("Fin du document");
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		System.out.printf("Ouverture de l'élément : %s %s %s\n", uri, localName, qName);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		System.out.printf("Fermeture de l'élément : %s %s %s\n", uri, localName, qName);
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		
		String text = new String(ch, start, length);
		System.out.println("Contenu textuel = " + text);
	}
}
