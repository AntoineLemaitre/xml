package Ex3;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class Main {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
			
		RSSReader RSS = new RSSReader();
		System.out.println(RSS.ReadFromURI("https://feed.infoq.com/"));
		handlerXML handlerXML = new handlerXML();
		
		SAXParserFactory saxParserFactory = SAXParserFactory.newDefaultInstance();
		saxParserFactory.setValidating(true);
		saxParserFactory.setNamespaceAware(true);
		SAXParser saxParser = saxParserFactory.newSAXParser();
		try (InputStream inputStream = new FileInputStream("src/main/resources/reponce");) {

			handlerXML handler = new handlerXML();
			saxParser.parse(inputStream, handler);
			
			

		} catch (IOException e) {
			e.printStackTrace();
		}


			
			

		

		System.out.println("Done!");

	}

}
