package Ex3;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RSSReader {
	
	public String ReadFromURI(String Url) {
		URI uri = URI.create(Url);
		//URI uri = URI.create("https://feed.infoq.com/");
		// ouverture d'un client
		HttpClient client = HttpClient.newBuilder().build();
		// création d'une requête
		HttpRequest request = HttpRequest.newBuilder().uri(uri).GET().build();
		// envoi de la requête
		HttpResponse<Stream<String>> send = null;
		try {
			send = client.send(request, HttpResponse.BodyHandlers.ofLines());
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// analyse de la réponse
		
		Stream<String> body = send.body();
		String xml = body.collect(Collectors.joining("\n"));
		System.out.println(xml);
		InputStream xmlFini = null;
		// je n'arrive pas a en faire un inputstream
	
		
		return xml;
		
	}

}
