package Ex2;

import Ex1.Employee;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Locale.Builder;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
public class XMLFile {
	
	Employee employee=new Employee();
	
	public Builder XMLFileService() {
		return new Builder();
	}
	public static class Builder {
		Employee employee=new Employee();
		
		public Builder write(Employee employe) {
			this.employee = employe;
			return this;
		}
		public void ToFile(String fileName) {
			Document document = DocumentHelper.createDocument();
			Element rootElement = document.addElement("user");
			rootElement.addAttribute("id", String.valueOf(employee.getId()));
			Element nameElement = rootElement.addElement("name");
			nameElement.addText(employee.getName());
			Element ageElement = rootElement.addElement("age");
			ageElement.addText(String.valueOf(employee.getAge()));

			try (Writer writer = new FileWriter(fileName);) {

				OutputFormat format = OutputFormat.createPrettyPrint();
				XMLWriter xmlWriter = new XMLWriter(writer, format);
				xmlWriter.write(document);

			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			
		}
		
		public Employee ReadFromFile(String fileName) {
			
			try (Reader reader = new FileReader(fileName);) {
				SAXReader saxReader = new SAXReader();
				Document document = saxReader.read(reader);
				
				Employee user = new Employee();
				Element rootElement = document.getRootElement();
				
				Attribute attributeID = rootElement.attribute("id");
				long id = Long.parseLong(attributeID.getValue());
				user.setId(id);
				
				
				
				user.setName(rootElement.elementText("name"));
				
				
				int age = Integer.parseInt(rootElement.elementText("age"));
				user.setAge(age);
				
				return user;
				
			} catch (IOException | DocumentException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	

}
